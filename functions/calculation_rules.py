"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))

# Add new functions for calculating metrics

# Berechnung KPI Preis

def kpi_price(system: LegoAssembly)->float: #Berechnung des Gesamtpreises als Summe der Einzelpreise
    total_price = 0 # Variable kreieren
    for i in system.get_component_list(-1):
        total_price += i.properties["price [Euro]"] # Aufsummieren der Preise aller Komponenten
    return total_price # Output des Gesamtpreises

# Berechnung KPI Lieferzeit

def kpi_deliv(system: LegoAssembly)->float: # Berechnung der Lieferzeit basierend auf der längsten Einzellieferzeit
    t_deliv = 0
    for i in system.get_component_list(-1):
        # Ermittlung der laengsten Lieferzeit
        if i.properties["delivery time [days]"] > t_deliv:
            t_deliv = i.properties["delivery time [days]"]
    return t_deliv

if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
